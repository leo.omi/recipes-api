# recipes-api

A simple CRUD API for a recipe application.

## Prerequisites
```
JDK 8
Maven
```
## Building and running
This project can be build with:

```
mvn package
```

After it is built, the project can be ran with:

```
java -jar target/recipes-api-0.0.1-SNAPSHOT.jar
```

The API will be accessible at http://localhost:8080

## Useful resources
An in-memory database, H2, is used in this project. It can be accessed at http://localhost:8080/h2-console. The JDBC URL is jdbc:h2:mem:testdb.

Swagger documentation with a GUI for the API can be accessed at http://localhost:8080/swagger-ui.html. The JSON can be accessed at http://localhost:8080/v2/api-docs.