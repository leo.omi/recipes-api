package com.example.recipesapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.recipesapi.model.Recipe;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long> {
  @Query("SELECT "
      + "new com.example.recipesapi.model.Recipe(r.id, r.name, r.calories, r.portions) "
      + "FROM recipe as r")
  List<Recipe> findAllSimplified();
}
