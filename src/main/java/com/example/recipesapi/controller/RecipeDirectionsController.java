package com.example.recipesapi.controller;

import java.net.URI;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.recipesapi.model.Directions;
import com.example.recipesapi.model.Recipe;
import com.example.recipesapi.repository.RecipeRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api")
@Api(value="Recipe directions operations",
  description="Operations pertaining to directions in recipes.")
public class RecipeDirectionsController {
  @Autowired
  private RecipeRepository recipeRepository;

  @ApiOperation(value = "Add or modify directions of a recipe.", response = Iterable.class)
  @PostMapping("/recipes/{id}/directions")
  public ResponseEntity<Object> saveDirection(@RequestBody Directions directions, @PathVariable long id) {
    Optional<Recipe> recipeOptional = recipeRepository.findById(id);

    if (!recipeOptional.isPresent())
      return ResponseEntity.notFound().build();

    Recipe recipe = recipeOptional.get();
    recipe.setDirections(directions.getDescription());

    recipeRepository.save(recipe);

    return ResponseEntity.ok(recipe);
  }
}
