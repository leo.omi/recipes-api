package com.example.recipesapi.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.recipesapi.model.Ingredient;
import com.example.recipesapi.model.Recipe;
import com.example.recipesapi.repository.RecipeRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api")
@Api(value="Recipe ingredients operations",
  description="Operations pertaining to ingredients in recipes.")
public class RecipeIngredientController {
  @Autowired
  private RecipeRepository recipeRepository;

  @ApiOperation(value = "Add a ingredient to an existing recipe", response = Iterable.class)
  @PostMapping("/recipes/{id}/ingredients")
  public ResponseEntity<Object> addIngredient(@RequestBody Ingredient ingredient, @PathVariable long id) {
    Optional<Recipe> recipeOptional = recipeRepository.findById(id);

    if (!recipeOptional.isPresent())
      return ResponseEntity.notFound().build();

    Recipe recipe = recipeOptional.get();
    List<Ingredient> ingredients = recipe.getIngredients();
    
    if (ingredients == null) {
      ingredients = new ArrayList<Ingredient>();
    }

    ingredients.add(ingredient);
    recipe.setIngredients(ingredients);
    recipeRepository.save(recipe);

    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(ingredient.getId())
        .toUri();
    return ResponseEntity.created(location).build();
  }
}
