package com.example.recipesapi.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.recipesapi.model.Recipe;
import com.example.recipesapi.repository.RecipeRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api")
@Api(value="Recipe operations",
  description="Operations pertaining to recipes.")
public class RecipeController {
  @Autowired
  private RecipeRepository recipeRepository;

  @ApiOperation(value = "View a list of all recipes with simplified details.", response = Iterable.class)  
  @GetMapping("/recipes")
  public List<Recipe> retrieveAllRecipes() {
    return recipeRepository.findAllSimplified();
  }

  @ApiOperation(value = "View a detailed recipe.", response = Iterable.class)
  @GetMapping("/recipes/{id}")
  public ResponseEntity<Object> retrieveRecipe(@PathVariable long id) {
    Optional<Recipe> recipeOptional = recipeRepository.findById(id);

    if (!recipeOptional.isPresent())
      return ResponseEntity.notFound().build();

    return ResponseEntity.ok(recipeOptional.get());
  }

  @ApiOperation(value = "Create a new recipe.", response = Iterable.class)
  @PostMapping("/recipes")
  public ResponseEntity<Object> createRecipe(@RequestBody Recipe recipe) {
    Recipe savedRecipe = recipeRepository.save(recipe);

    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedRecipe.getId())
        .toUri();

    return ResponseEntity.created(location).build();
  }

  @ApiOperation(value = "Update or create a recipe.", response = Iterable.class)
  @PutMapping("/recipes/{id}")
  public ResponseEntity<Object> updateRecipe(@RequestBody Recipe recipe, @PathVariable long id) {

    Optional<Recipe> recipeOptional = recipeRepository.findById(id);

    if (!recipeOptional.isPresent())
      return ResponseEntity.notFound().build();

    recipe.setId(id);

    recipeRepository.save(recipe);

    return ResponseEntity.noContent().build();
  }
}
