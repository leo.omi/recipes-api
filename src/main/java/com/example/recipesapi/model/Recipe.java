package com.example.recipesapi.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity(name = "recipe")
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Recipe {
  @Id
  @GeneratedValue
  private Long id;

  private String name;

  private Double calories;

  private Long portions;

//  @OneToMany(cascade = {CascadeType.ALL})
//  @JoinColumn(name="recipe_id")
//  private List<Direction> directions;

  @Column(columnDefinition="CLOB")
  private String directions;

  @OneToMany(cascade = {CascadeType.ALL})
  @JoinColumn(name="recipe_id")
  private List<Ingredient> ingredients;

  public Recipe() {
    super();
  }

  public Recipe(Long id, String name, Double calories, Long portions) {
    super();
    this.id = id;
    this.name = name;
    this.calories = calories;
    this.portions = portions;
  }

  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public Double getCalories() {
    return calories;
  }
  public void setCalories(Double calories) {
    this.calories = calories;
  }
  public Long getPortions() {
    return portions;
  }
  public void setPortions(Long portions) {
    this.portions = portions;
  }
  public String getDirections() {
    return directions;
  }
  public void setDirections(String directions) {
    this.directions = directions;
  }
//  public List<Direction> getDirections() {
//    return directions;
//  }
//  public void setDirections(List<Direction> directions) {
//    this.directions = directions;
//  }
  public List<Ingredient> getIngredients() {
    return ingredients;
  }
  public void setIngredients(List<Ingredient> ingredients) {
    this.ingredients = ingredients;
  }
}
